/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mnist_example;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.canova.api.records.reader.RecordReader;
import org.canova.api.records.reader.impl.CSVRecordReader;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.spark.canova.RecordReaderFunction;
import org.deeplearning4j.spark.impl.multilayer.SparkDl4jMultiLayer;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.lossfunctions.LossFunctions;

/**
 *
 * @author ibrahim
 */
public class irisdataset_example {
    public static void main(String[] args) throws Exception {
        SparkConf sparkConf = new SparkConf();
        sparkConf.setMaster("local[*]");
        sparkConf.setAppName("Iris");

        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        RecordReader recordReader = new CSVRecordReader(0, ",");
        JavaRDD<String> irisDataLines =sc.textFile("/home/ibrahim/NetBeansProjects/Mnist_example/src/main/resources/iris2.txt");
        int labelIndex = 4;
        int numOutputClasses = 3;
        JavaRDD<DataSet> trainingData = irisDataLines.map(new RecordReaderFunction(recordReader, labelIndex, numOutputClasses));

        //Create and initialize multi-layer network
        final int numInputs = 4;
        int outputNum = 3;
        int iterations = 1;

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(12345)
                .iterations(iterations)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .learningRate(1e-1)
                .l1(0.01).regularization(true).l2(1e-3)
                .list()
                .layer(0, new DenseLayer.Builder().nIn(numInputs).nOut(3)
                        .activation("tanh")
                        .weightInit(WeightInit.XAVIER)
                        .build())
                .layer(1, new DenseLayer.Builder().nIn(3).nOut(2)
                        .activation("tanh")
                        .weightInit(WeightInit.XAVIER)
                        .build())
                .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                        .weightInit(WeightInit.XAVIER)
                        .activation("softmax")
                        .nIn(2).nOut(outputNum).build())
                .backprop(true).pretrain(false)
                .build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();
        net.setUpdater(null);   //Workaround for a minor bug in 0.4-rc3.8

        //Create Spark network
        SparkDl4jMultiLayer sparkNetwork = new SparkDl4jMultiLayer(sc, net);

        int nEpochs = 6;

        List<float[]> temp = new ArrayList<float[]>();
        for (int i = 0; i < nEpochs; i++) {
            MultiLayerNetwork network = sparkNetwork.fitDataSet(trainingData);
            temp.add(network.params().data().asFloat().clone());
            System.out.println("*******************************************************************temp lenght ="+temp.get(i).length);
        }
        File cıktı = new File("/home/ibrahim/Desktop/staj/irisout.txt");
        if (!cıktı.exists()) {
            cıktı.createNewFile();
        }
        PrintWriter yaz= new PrintWriter (cıktı);
       System.out.println("Parameters vs. iteration: ");
        for (int i = 0; i < temp.size(); i++) {
            yaz.println(i + "\t " + Arrays.toString(temp.get(i)));
            
        }
       
        yaz.close();
    }

    //This approach: isn't suitable for large data, but should work ok here for Iris (150 examples)
    
    }


